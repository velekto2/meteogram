import requests
import json
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import datetime
import io
from opencage.geocoder import OpenCageGeocode
import unittest

from meteogram import geolocate, get_api_data


class ApiDataTest(unittest.TestCase):
    def test_invalid_coords(self):
        with self.assertRaises(requests.HTTPError):
            get_api_data({"coords": (300, 400), "tz_offset": 0.0, "name": ""})

    def test_empty_coords(self):
        with self.assertRaises(requests.HTTPError):
            get_api_data({"coords": (None, None), "tz_offset": 0.0, "name": ""})

    def test_no_coords(self):
        with self.assertRaises(KeyError):
            get_api_data({"tz_offset": 0.0, "name": ""})


class GeolocationTest(unittest.TestCase):
    def test_invalid_place(self):
        with self.assertRaises(KeyError, msg="location not found"):
            geolocate("shkdglsrgdhfjlg")

    def test_valid_place(self):
        res = geolocate("Kathmandu, Nepal")
        self.assertNotEqual(res["coords"], (None, None))
        self.assertNotEqual(res["tz_offset"], None)
        self.assertNotEqual(res["name"], None)


if __name__ == "__main__":
    unittest.main()
