# Zápočtový projekt: Meteogram

Zápočtový projekt na předmět Vědecké programování v Pythonu - grafické znázornění předpovědi počasí v zadané lokaci. Program získává data skrze API norského Meteorologického institutu. Protože program dále využívá OpenCage API pro geocoding, je potřeba uložit API klíč do souboru `api_key.json` ve formátu
```
{
    "opencage_api_key": "00000000000000000000000000000000"
}
```
Seznam potřebných balíčků je uveden v `required_packages.txt`.

![Relatable image](relatable_image.png)
