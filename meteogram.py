import requests
import json
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import datetime
import io
from opencage.geocoder import OpenCageGeocode


def geolocate(place: str) -> dict:
    """Takes user-inputted place, returns coordinates, offset of local time from UTC, and formatted location name."""
    with open("api_key.json", "rt", encoding="utf-8") as f:
        api_key = json.loads(f.read())["opencage_api_key"]
    geocoder = OpenCageGeocode(api_key)
    results = geocoder.geocode(place, limit=1, language="cs")
    try:
        results[0]
    except IndexError:
        raise KeyError("location not found")
    coords = (results[0]["geometry"]["lat"], results[0]["geometry"]["lng"])
    tz_offset = (
        results[0]["annotations"]["timezone"]["offset_sec"] / 3600
    )  # API returns offset in seconds
    name = results[0]["formatted"]
    return {"coords": coords, "tz_offset": tz_offset, "name": name}


def get_api_data(location: dict) -> dict:
    """Downloads data from the API and returns the relevant parts as a tuple of two dictionaries: data types and their units, and data points in specific times."""
    coords = location["coords"]
    local_tz_offset = location["tz_offset"]
    response = requests.get(
        f"https://api.met.no/weatherapi/locationforecast/2.0/complete?lat={coords[0]}&lon={coords[1]}",
        timeout=60,
        headers={"user-agent": "velekto2-meteogram"},
    )
    response.raise_for_status()  # raise an exception if 4xx or 5xx status code is returned
    content = json.loads(response.content)
    units = content["properties"]["meta"]["units"]
    data = dict()
    for i in content["properties"]["timeseries"]:
        if (
            not "next_1_hours" in i["data"]
        ):  # the last data point doesn't include an hourly forcast
            break
        local_time = (
            datetime.datetime.strptime(i["time"], "%Y-%m-%dT%H:%M:%SZ")
            + datetime.timedelta(hours=local_tz_offset)
        ).strftime(
            "%Y-%m-%dT%H:%M:%S"
        )  # convert timedate string to local time
        data[local_time] = (
            i["data"]["instant"]["details"]
            | i["data"]["next_1_hours"]["summary"]
            | i["data"]["next_1_hours"]["details"]
        )
    return {"units": units, "data": data, "name": location["name"]}


def generate_meteogram_plot(units_data: dict):
    """Returns the meteogram as a matplotlib figure."""
    time_values = list(units_data["data"].keys())
    airtemp_values = [i["air_temperature"] for i in list(units_data["data"].values())]
    rain_values = [i["precipitation_amount"] for i in list(units_data["data"].values())]
    data = [
        (time_values[i], airtemp_values[i], rain_values[i])
        for i in range(len(time_values))
    ]
    data_np = np.array(
        data, dtype=[("time", "datetime64[s]"), ("airtemp", float), ("rain", float)]
    )
    # air temperature subplot
    fig, ax_airtemp = plt.subplots()
    min_time = min(data_np["time"])
    max_time = max(data_np["time"])
    ax_airtemp.set_xlim(min_time, max_time)
    min_airtemp = min(data_np["airtemp"])
    max_airtemp = max(data_np["airtemp"])
    delta_airtemp = max_airtemp - min_airtemp
    ax_airtemp.set_ylim(
        min_airtemp - 0.1 * delta_airtemp, max_airtemp + 0.1 * delta_airtemp
    )
    ax_airtemp.xaxis.set_major_locator(mdates.DayLocator())
    ax_airtemp.xaxis.set_minor_locator(mdates.HourLocator(byhour=[6, 12, 18]))
    ax_airtemp.grid(True, which="major", axis="x", color="0.6")
    ax_airtemp.grid(True, which="minor", axis="x", color="0.9")
    ax_airtemp.plot("time", "airtemp", data=data_np, color="C3")
    ax_airtemp.xaxis.set_major_formatter(mdates.DateFormatter("%-d. %-m."))
    ax_airtemp.xaxis.set_minor_formatter(mdates.DateFormatter("%-H:%M"))
    for label in ax_airtemp.get_xticklabels(which="minor"):
        label.set(rotation=90, horizontalalignment="right")
    ax_airtemp.set_title("Meteogram: " + units_data["name"])
    ax_airtemp.set_ylabel("Teplota [˚C]")
    fig.canvas.manager.set_window_title("Meteogram: " + units_data["name"])
    # precipitation subplot
    ax_rain = ax_airtemp.twinx()
    max_rain = max(data_np["rain"])
    ax_rain.set_ylim(0, 0.1 + max_rain * 3)
    ax_rain.bar("time", "rain", data=data_np, width=0.05, color="C0")
    ax_rain.set_ylabel("Srážky [mm]")
    return fig


if __name__ == "__main__":
    print("* Zápočtový projekt na PYTH: Meteogram *")
    while True:
        print("Zadej místo, nebo zanech prázdné pro ukončení:")
        place = input()
        if not place:
            break
        try:
            met = generate_meteogram_plot(get_api_data(geolocate(place)))
            met.show()
        except KeyError as exc:
            if str(exc) == str(KeyError("location not found")):
                print("Místo nebylo nalezeno.")
            else:
                raise exc
